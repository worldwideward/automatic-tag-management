# Automatic tag management

This repository aims to guide you to set up automatic tag management in Gitlab.

## Workflow for a new project

**Initiate repository**

- Create a new repository, and cofigure it (see repository settings)
- clone the empty repository to your local machine and add the `.gitlab-ci.yml` file
- commit and push to `main`

**Developing the application**

1. create a Gitlab issue (or open an existing issue), and create a new branch using the button (this way you automatically respect the naming convention)
2. follow the onscreen instructions to checkout the new branch on your local machine
3. do some commits and push them to the new branch
4. if you are satisfied, open a merge request: now the application will be built and tested
5. if the pipeline passes you can continue the merge (this will automatically close the merge request): a new tag will be created and pushed to the repository
6. the tag creation will trigger the release job, that tags your application with the created tag, and pushes it to some kind of artifact registry

## Scripts

There are two simple scripts that help with tagging, they should/could be implemented in the gitlab-runner.

The `git-tags.sh` script calls the `semver-tags.sh` script, so without each other, auto tagging will not work.

```
git-tags.sh [REMOTE] [SEMVER CHANGE TYPE]

example: git-tags.sh "https://user:personal_access_token@gitlab.com/group/project.git" "MINOR"

semver-tags.sh [CURRENT TAG] [CHANGE TYPE]

example: semver-tags.sh "v0.1.0" "MINOR"
```

## Overwriting the default semver change type

To overwrite the default semver change type "PATCH", add the variable `CI_CHANGE_TYPE` to the repository as a CI/CD variable. Choose MINOR or MAJOR as possible values.

## Workflow for an existing project

To add this functionality to an existing project: 

1. copy the `auto-tag-job.yml` to your ci/cd repository/directory and include it in your `.gitlab-ci.yml`. Optionally adjust the branch for which the job runs.
2. copy the scripts, `gitlab-tags.sh` and `semver-tags.sh` to the same destination.
3. make sure your `.gitlab-ci.yml` has a `release` stage defined
4. make sure either:

- your repository does not have any existing git tags
- your repository only has pre-existing semver compliant tags
- you created a semver compliant tag as the most recent tag on the main branch.

5. you should now be able to run the pipeline on the next commit, and have tags automatically created on each (merge) commit.

