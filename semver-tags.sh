#!/bin/bash

# Generate the next semver tag

# Arguments
#
CURRENT_TAG=$1
CHANGE_TYPE=$2

# Help message 
#
if [ "$CURRENT_TAG" == "--help" ]; then

	echo "Usage: semver-tags.sh [current tag] [change type]"
	echo " "
	echo "current tag:    make sure it is semver compliant, eg v0.1.0 or 0.1.0"
	echo "change type:    choose which number to increment; MAJOR, MINOR or PATCH"
	echo " "
	echo "Note: execute without arguments to generate a default tag"
	echo " "
	exit 0

elif [ -z "$CURRENT_TAG" ]; then

	CURRENT_TAG="v0.1.0"
	CHANGE_TYPE="MINOR"
	echo $CURRENT_TAG
	exit 0

elif [ -z $CHANGE_TYPE ]; then

	echo "[ERROR] Please specificy the change type: MAJOR, MINOR or PATCH"
	exit 1
fi

# Functions
#
function checkSemverTag() { # check if the semver tag supplied is compliant

	tag=$1
	
	# this will the maximum tag to be either v999.999.999 or 9999.999.999
	check=$(echo $tag | grep -E '^[v|0-9][0-9]{0,3}\.[0-9]{1,3}\.[0-9]{1,3}$' | wc -c)

	if [ "$check" == 0 ]; then
		
		echo "false"
	else
		echo "true"
	fi
}

function semverToNumbers() { # convert the semver tag to an arry of numbers

	tag=$1
	strip_v=$(echo $tag | sed 's/^v//')
	version_numbers=()

	major=$(echo $strip_v | cut -d "." -f 1)
	minor=$(echo $strip_v | cut -d "." -f 2)
	patch=$(echo $strip_v | cut -d "." -f 3)

	version_numbers+=($major)
	version_numbers+=($minor)
	version_numbers+=($patch)

	echo ${version_numbers[@]}
}

# Logic
#
compliance=$(checkSemverTag $CURRENT_TAG)

if [ "$compliance" == "false" ]; then 

	echo "[ERROR] Tag $CURRENT_TAG is not semver compliant"
	exit 1; 
fi

semverNumbers=($(semverToNumbers $CURRENT_TAG))

case $CHANGE_TYPE in

	MAJOR|major)

		increment=$(expr ${semverNumbers[0]} + 1)
		echo "v$increment.0.0"
		;;
	MINOR|minor)

		increment=$(expr ${semverNumbers[1]} + 1)
		echo "v${semverNumbers[0]}.$increment.0"
		;;
	PATCH|patch)

		increment=$(expr ${semverNumbers[2]} + 1)
		echo "v${semverNumbers[0]}.${semverNumbers[1]}.$increment"
		;;
	*)
		echo "[ERROR] Bogus change type, $CHANGE_TYPE doesn't match MAJOR , MINOR or PATCH"
		exit 1
		;;
esac

exit 0
