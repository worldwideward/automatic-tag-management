#!/bin/bash

GIT_REMOTE="$1"
SEMVER_CHANGE_TYPE="$2"

LATEST_TAG=$(git tag --sort=-committerdate | head -n1)
CURRENT_TAGS=($(git tag --points-at))

function gitTagAndPush() {

	GIT_REMOTE=$1
	GIT_TAG=$2

	git remote add tag-origin $GIT_REMOTE
	git tag $GIT_TAG
	git push tag-origin $GIT_TAG
}

function setInitialTag() {

	NEW_TAG=$(./semver-tags.sh)
	gitTagAndPush $GIT_REMOTE $NEW_TAG
}

if [ -z $LATEST_TAG ]; then

	echo "[INFO] No tags for this repository"
	setInitialTag
	exit 0
fi

if [ -z $CURRENT_TAGS ]; then

	echo "[INFO] No tags for this commit"

	./semver-tags.sh $LATEST_TAG "$SEMVER_CHANGE_TYPE"
	exit_code=$?

	if [ "$exit_code" -eq 0 ]; then

		echo $SEMVER_CHANGE_TYPE

		NEW_TAG=$(./semver-tags.sh $LATEST_TAG "$SEMVER_CHANGE_TYPE")
		gitTagAndPush $GIT_REMOTE $NEW_TAG
	else
		#setInitialTag # optional
		exit $exit_code
	fi
	exit 0

else 
	echo "[ERROR] This commit is already tagged with tag ${CURRENT_TAGS[@]}"
	exit 1
fi
